var exec = require('sync-exec');

function getServiceStatus() {
	//return 'sickrage start/running, process 9955';
	return exec('initctl list | grep sickrage').stdout;
}

exports.getSickrageRunning = function () {
	var status = getServiceStatus();
	
	var startIndex = status.indexOf('/') + 1;
	var endIndex = status.indexOf(',');
	
	if (endIndex === -1) {
		endIndex = status.length;
	}
	
	return status.substring(startIndex, endIndex);
};

exports.getSickragePid = function () {
	if (this.getSickrageRunning() === 'running') {
		var status = getServiceStatus();
		
		var startIndex = status.lastIndexOf(' ');
		var endIndex = status.length;

		return parseInt(status.substring(startIndex, endIndex));
	}

	return 0;
};

exports.getSickrageUsage = function () {
	var pid = this.getSickragePid();

	if (pid === 0) {
		return '';
	}

	var usage = exec('ps -p ' + pid + ' -o %cpu,%mem,cmd').stdout;
	//var usage = '%CPU %MEM CMD\n 0.4  3.0 python /home/user/SickRage/SickBeard.py';
	
	usage = usage.replace('\n', '<br />');

	return usage;
};

exports.startSickrage = function () {
	return exec('start sickrage').stdout;
};

exports.stopSickrage = function () {
	return exec('stop sickrage').stdout;
};
