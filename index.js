'use strict';

var http = require('http');
var express = require('express');
var fs = require('fs');
var exphbs = require('express-handlebars');

var sickrage = require('./modules/sickrage.js');

var app = express();

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');
app.use(express.static('public'));

var port = process.env.SERVER_PORT || 80;
var iface = process.env.SERVER_IFACE || null;

var server = app.listen(port, iface);

app.get('/', function(req, res) {
    res.render('index', {title: 'Home'});
});

app.get('/sickrage', function(req, res) {
    res.render('sickrage', {title: 'SiCKRAGE'}); 
});

app.get('/sickrage/running', function (req, res) {
	res.send(sickrage.getSickrageRunning());
});

app.get('/sickrage/pid', function (req, res) {
	res.send(sickrage.getSickragePid().toString());
});

app.get('/sickrage/usage', function (req, res) {
	res.send(sickrage.getSickrageUsage());
});

app.get('/sickrage/start', function (req, res) {
	res.send(sickrage.startSickrage());
});

app.get('/sickrage/stop', function (req, res) {
	res.send(sickrage.stopSickrage());
});
